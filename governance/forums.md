# Forums

Feature Requests: https://talk.tiddlywiki.org (this is a placeholder)

Discussion Group: https://talk.tiddlywiki.org

Community curated links: https://links.tiddlywiki.com/

Chat: https://gitter.im/TiddlyWiki/public  

Voice Chat: https://discord.gg/d7vf7Vdc

TiddlyWiki on Reddit at https://www.reddit.com/r/TiddlyWiki5/

Twitter: Follow @TiddlyWiki on Twitter and view recent #tiddlywiki posts.

TiddlyWiki Core Discussions: https://github.com/Jermolene/TiddlyWiki5/discussions

Issues: https://github.com/Jermolene/TiddlyWiki5/issues
