# Contributing Ideas

(TODO more details. links are placeholders atm)

see: https://gitlab.com/tiddlywiki.org/senatus/committee/-/blob/master/governance/forums.md

# Contributing Documentation

see: https://github.com/Jermolene/TiddlyWiki5/blob/master/contributing.md (TODO should be easy for _non_ developers!)

# Contributing Code

see: https://github.com/Jermolene/TiddlyWiki5/blob/master/contributing.md
