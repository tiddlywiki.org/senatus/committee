
# Community Guidelines

We want to keep the TiddlyWiki community an interactive, open place to participate, but we need your help. We expect that you, as a participant of the TiddlyWiki community, will:

 - Be courteous and polite to fellow community members.

 - Respect other people; no racial, gender or other abuse will be tolerated.

 - Not swear.

 - Ensure that you have legal rights for posting your content and are not violating any copyright, trade secrets, trademark, or other proprietary rights.

 - Make sure your posts are sent to the appropriate channels and are relevant to the discussion.

 - Read and review the [Privacy Policy.](./privacy-policy.md)

**We reserve the right to remove major offenders.**

The Terms and Conditions and the Privacy Policy apply across all areas of the website. 