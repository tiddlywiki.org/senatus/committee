!!!! (TODO) improve this and make it consistent !!!!

# Who Should Contribute?

At TiddlyWiki.org, not only developers with different interests, but also **users** should participate!

## Users

Enthusiast users can [help with documentation](https://gitlab.com/tiddlywiki.org/docs), help with testing (TODO) and [submit bugs](https://gitlab.com/tiddlywiki.org/core/tiddlywiki/issues), and [discuss features.](https://gitlab.com/tiddlywiki.org/feature-request)

Come discuss with us on [TiddlyWiki forum](https://groups.google.com/forum/#!forum/tiddlywiki)

## UI Developers

TiddlyWiki user experience is delivered by tiddlywiki wiki-text, which is translated to HTML and CSS. The basic building blocks are widgets, which are provided by the [core-developers](https://gitlab.com/tiddlywiki.org/core). Widgets are based on JavaScript. 

## App Developers

JavaScript developers can add new features and integrate other open source technologies to enhance the user experience.

Developers can enhance our Core application, using the [plugin mechanism](https://gitlab.com/tiddlywiki.org/plugins), which allows tiddlywiki to include other frameworks. 

